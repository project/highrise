<?php

require_once("includes/Highrise.class.php");

/*
 * Creates Admin settings form.
 */
function highrise_admin_settings() {
  $form['highrise_account'] = array(
      '#type' => 'textfield',
      '#title' => t('Highrise account name'),
      '#default_value' =>  variable_get('highrise_account', ''),
      '#description' => t('The account name of highrise that will post the data.(https://&lt;<b>accountname</b>&gt;.highrisehq.com).'),
  );

  $form['highrise_token'] = array(
      '#type' => 'textfield',
      '#title' => t('Highrise authentication token'),
      '#default_value' =>  variable_get('highrise_token', ''),
      '#description' => t('The authentication token to validate against the accountname.  Note:  you\'ll find on the "My Info" screen in Highrise (click the "Reveal authentication token for feeds/API" link).'),
  );
  
  $form['highrise_tag'] = array(
      '#type' => 'textfield',
      '#title' => t('Highrise tag'),
      '#default_value' =>  variable_get('highrise_tag', ''),
      '#description' => t('The tag to be associated with the contact in highrise.'),
  );
  $form['#validate'][] = 'highrise_admin_settings_validate';
  return system_settings_form($form);
}

/*
 * Adds a validate handler to the settings form
 */

function highrise_admin_settings_validate($form, &$form_state) {
  $hr = new Highrise();
  $hr->debug = FALSE;
  $account = $form_state['values']['highrise_account'];
  $token = $form_state['values']['highrise_token'];
  $hr->setAccount($account);
  $hr->setToken($token);
  $my_info = $hr->findMe();
  if (!isset($my_info->id)) {
    drupal_set_message('Account details are not valid. Please enter again.', 'error');
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/*
 * Displays current mappings.
 */
function highrise_configure() {
  $output = "<p>" . t('No Mapping defined yet.') . "</p>";
  $header = array(
  array('data' => t('Mapper Name'), 'field' => 'd.domain_id'),
  array('data' => t('Webform'), 'field' => 'd.subdomain'),
  );
  $rows = highrise_getwebforms();
  $output .= theme_table($header, $rows, array('id' => 'domain-list'));
  return $output;
}

/*
 * Allows to create mappings for a webform.
 */
function highrise_mapping() {
  drupal_add_js(drupal_get_path('module', 'highrise') . '/lib/highrise.admin.js', 'module', 'footer', FALSE, TRUE, FALSE);
  $highrise_fields = highrise_getfields();
  $webform_option = highrise_mapping_form();

  $form['highrise_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Mapper Name'),
    '#weight' => -1
  );
  $form['webform'] = array(
    '#type' => 'select',
    '#title' => 'Select a Webform',
    '#options' => $webform_option
  );

  foreach ($highrise_fields as $fields) {
    if (in_array($fields['nid'], array(1, 2))) {
      $required = TRUE;
    }
    else {
      $required = FALSE;
    }
    $form[$fields['field_name']] = array(
      '#type' => 'textfield',
      '#required' => $required,
      '#title' => t($fields['field_description']),
      '#autocomplete_path' => 'admin/settings/highrise/autocomplete'
      );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#default_value' =>  t('Save'),
  );
  $form['#submit'][] = '_highrise_save';
  return $form;

}

/*
 * Displays webform listing to map with,
 * except the one which has been previously mapped.
 */
function highrise_mapping_form() {
  $mappings = array();
  $sql = db_query("SELECT n.nid , n.title
            FROM node n
            INNER JOIN {node_revisions} r ON n.vid = r.vid
            WHERE n.type = '%s'
            AND n.nid NOT IN (SELECT nid FROM {highrise_webforms})", 'webform');
  while ($rows = db_fetch_object($sql)) {
    $mappings[$rows->nid] = $rows->title;
  }
  return $mappings;
}

function highrise_getfields() {
  $result = db_query(db_rewrite_sql("SELECT * FROM {highrise_fields}"));
  while ($obj = db_fetch_object($result)) {
    $items[] = array('nid' => $obj->hid, 'field_name' => $obj->name, 'field_description' => $obj->description);
  }
  return $items;
}

/*
 * Callback function to fetch component id.
 */
function highrise_autocomplete($string = '') {
  if ($string) {
    $result = db_query('SELECT cid, name FROM {webform_component} WHERE name LIKE "%s%%" and nid = %d', arg(5), $string);
    while ($obj = db_fetch_object($result)) {
      $key = $obj->name . ' [cid:'. $obj->cid .']';
      $items[$key] = $obj->name;
    }
    print drupal_to_js($items);
    exit();
  }
}

function highrise_lookup($webformid) {
  $result = db_query('SELECT cid,name FROM {webform_component} WHERE nid = %d', $webformid);
  while ($obj = db_fetch_object($result)) {
    $items[$obj->name] = check_plain($obj->cid);
  }
  return $items;
}

/*
 * Returns list of current mappings.
 */
function highrise_getwebforms() {
  $query = "SELECT highrise_webforms.name, highrise_webforms.nid, node.title from {highrise_webforms} LEFT JOIN {node} on highrise_webforms.nid = node.nid";
  $result = db_query($query);
  while ($object = db_fetch_object($result)) {
    $nodes[] = $object;
    $row = array();
    $row['name'] = $object->name;
    $row['title'] = $object->title;
    $row['delete'] = l('delete', 'highrise/delete/' . $object->nid);
    $rows = array();
    $rows[] = $row;
  }

  return $rows;
}

function _highrise_save($form, &$form_state) {
  $webform_id = $form_state['values']['webform']; // Webform ID (Contact Us)
  $mapping_name = $form_state['values']['highrise_name']; // Mapping Name (Machine-name)
  db_query("INSERT INTO {highrise_webforms} (nid, name) VALUES (%d, '%s')", $webform_id, $mapping_name);
  $mid = db_last_insert_id('highrise_webforms', 'mid');
  $fields = array();
  $fields[] = $form_state['values']['first-name'];
  $fields[] = $form_state['values']['last-name'];
  $fields[] = $form_state['values']['title'];
  $fields[] = $form_state['values']['background'];
  $fields[] = $form_state['values']['email-address'];
  $fields[] = $form_state['values']['company-name'];
  $fields[] = $form_state['values']['phone-number'];

  $cids = highrise_filter_cid($fields);
  $hids_array = array('first-name', 'last-name', 'title', 'background', 'email-address', 'company-name',  'phone-number');
  $hids_selected = array();
  foreach ($fields as $key => $value) {
    if (!empty($value)) {
      $hids_selected[] = $hids_array[$key];
    }
  }
  
  $hids = highrise_fetch_hid($hids_selected);
  $query = '';
  $query = 'INSERT INTO {highrise_mappings} VALUES ';
  for ($i = 0; $i < sizeof($cids); $i++ ) {
    $query .= '(' . $mid . ',' . $cids[$i] . ',' . $hids[$i] . ')';
    if ($i < sizeof($cids) -1) {
      $query .= ',';
    }
  }
  db_query($query);
  drupal_set_message(t('Mapping has been saved.'));
}

/*
 * Filters the cid value.
 */
function highrise_filter_cid($fields) {
  foreach ($fields as $key)
  if (!empty($key)) {
    $key = drupal_explode_tags($key);
    foreach ($key as $v) {
      $cid = NULL;
      preg_match('/^(?:\s*|(.*) )?\[\s*cid\s*:\s*(\d+)\s*\]$/', $v, $matches);
      if (!empty($matches)) {
        // Explicit [cid:n].
        list(, $title, $cid) = $matches;
      }
      if (!empty($cid)) {
        $cids[] = $cid;
      }
    }
  }
  return $cids;
}

/*
 * Returns list of highrise field ids.
 */
function highrise_fetch_hid($vals) {
  $hids = array();
  foreach ($vals as $key) {
    $hids[] = db_result(db_query("SELECT hid FROM {highrise_fields} WHERE name='%s'", $key));
  }
  return $hids;
}

/*
 * Delete webform mapping.
 */
function highrise_mapping_delete(&$form_state, $mapp_id) {
  $form['#mapp_id'] = $mapp_id;
  return confirm_form($form, t('Are you sure you want to delete this mapping?'), 'admin/settings/highrise/mapping', t('This action cannot be undone.'), t('Delete'));
}

function highrise_mapping_delete_submit($form, &$form_state) {
  $form_id = $form['#mapp_id'];
  $mid =  db_result(db_query("SELECT mid from {highrise_webforms} WHERE nid = %d", $form_id));
  if ($mid) {
    db_query("DELETE FROM {highrise_mappings} WHERE mid = %d", $mid);
    db_query("DELETE FROM {highrise_webforms} WHERE mid = %d", $mid);
  }
  drupal_set_message(t('Mapping has been deleted.'));
  $form_state['redirect'] = 'admin/settings/highrise/mapping'; 
}
